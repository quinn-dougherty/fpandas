#!/usr/bin/env python

from setuptools import setup  # type: ignore

with open('README.md', 'r') as f:
    long_description = f.read()

setup(name='fpandas',
      version='0.6',
      description='Utils toward a monad-inspired pandas design-pattern.',

      #long_description=long_description,
      #long_description_content_type="text/markdown",
     
      url='https://gitlab.com/quinn-dougherty/fpandas',
      author='Quinn Dougherty',
      author_email='quinn.dougherty92@gmail.com',
      license='MIT',
      packages=['fpandas'],
      install_requires=[
          'pandas'
      ],
      zip_safe=False)
